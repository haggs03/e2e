#ifndef E2E_P01_H
#define E2E_P01_H

#define E2E_E_OK             0x00
#define E2E_E_INPUTERR_NULL  0x13
#define E2E_E_INPUTERR_WRONG 0x17
#define E2E_E_INTERR         0x19
#define E2E_E_WRONGSTATE     0x1A

/**
 * @brief The Data ID is two bytes long in E2E Profile 1.
 * 
 * There are four inclusion modes how the implicit two-byte Data ID is included in the one-byte CRC.
 */
typedef enum {
    E2E_P01_DATAID_BOTH   = 0,  /**< Two bytes are included in the CRC */
    E2E_P01_DATAID_ALT    = 1,  /**< One of the two bytes is included, alternating, depending on counter parity */
    E2E_P01_DATAID_LOW    = 2,  /**< Only the low byte is included, the high byte is never used. */
    E2E_P01_DATAID_NIBBLE = 3   /**< The low byte is included in the CRC, the low nibble of the high byte is sent with the data */
} E2E_P01DataIDMode;

/**
 * @brief Configuration of transmitted Data (Data Element or I-PDU), for E2E Profile 1.
 * For each transmitted Data, there is an instance of this typedef.
 */
typedef struct {
    uint16 CounterOffset;           /**< Bit offset of Counter in MSB first order. */
    uint16 CRCOffset;               /**< Bit offset of CRC (i.e. since *Data) in MSB first order. */
    uint16 DataID;                  /**< A unique identifier, for protection against masquerading. */
    uint16 DataIDNibbleOffset;      /**< Bit offset of the low nibble of the high byte of Data ID. */
    E2E_P01DataIDMode DataIDMode;   /**< Inclusion mode of ID in CRC computation */
    uint16 DataLength;              /**< Length of data, in bits. */
    uint8 MaxDeltaCounterInit;      /**< Initial maximum allowed gap between two counter values of two consecutively received valid Data. */
    uint8 MaxNoNewOrRepeatedData;   /**< The maximum amount of missing or repeated Data which the receiver does not expect to exceed */
    uint8 SyncCounterInit;          /**< Number of Data required for validating the counter that must be received after an unexpected behaviour*/
} E2E_P01ConfigType;

/**
 * @brief State of the sender for a Data protected with E2E Profile 1.
 */
typedef struct {
    uint8 Counter;  /**< Counter to be used for protecting the next Data. */
} E2E_P01ProtectStateType;

/**
 * @brief Result of the verification of the Data in E2E Profile 1, determined by the Check function.
 */
typedef enum {
    E2E_P01STATUS_OK            = 0x00, /**< OK: The new data has been received: CRC correct, Counter incremented, no Data has been lost. */
    E2E_P01STATUS_NONEWDATA     = 0x01, /**< ERROR: Check() function has been invoked but no new Data is available. */
    E2E_P01STATUS_WRONGCRC      = 0x02, /**< ERROR: The new data has been received but CRC is incorrect. */
    E2E_P01STATUS_SYNC          = 0x03, /**< NOT VALID: The new data has been received after unexpected behavior of counter: CRC OK, but counter check not finalized yet. */
    E2E_P01STATUS_INITIAL       = 0x04, /**< INIT: The new data has been received, CRC OK but counter cannot be verified since it is the first Data. */
    E2E_P01STATUS_REPEATED      = 0x08, /**< ERROR: The new data has been received, CRC OK but counter is identical to the most recent Data received. */
    E2E_P01STATUS_OKSOMELOST    = 0x20, /**< OK: The new data has been received, CRC OK but some data was probably lost since the last correct reception. */
    E2E_P01STATUS_WRONGSEQUENCE = 0x40  /**< ERROR: The new data has been received, CRC OK but too much data was probably lost since the last correct reception. */
} E2E_P01CheckStatusType;

/**
 * @brief State of the receiver for a Data protected with E2E Profile 1.
 */
typedef struct {
    uint8 LastValidCounter;             /**< Counter value most recently received. */
    uint8 MaxDeltaCounter;              /**< Specifies the max allowed difference between two counter values of consecutively received valid messeges. */
    boolean WaitForFirstData;           /**< If true, no correct data gas beet yet received after the receiver initialization. */
    boolean NewDataAvailable;           /**< Indicates that a new data is available to be checked. */
    uint8 LostData;                     /**< Number of data lost since reception of last valid one. */
    E2E_P01CheckStatusType Status;      /**< Result of the verification of the Data. */
    uint8 SyncCounter;                  /**< Number of Data required for validating the counter that must be received after an unexpected behaviour. */
    uint8 NoNewOrRepeatedDataCounter;   /**< Amount of consecutive reception cycles in which either there was no new data or data was repeated. */
} E2E_P01CheckStateType;

/**
 * @brief Initializes the protection state.
 * @param [out] StatePtr Pointer to port/data communication state.
 * @retval E_OK Function executed successfully.
 */
Std_ReturnType E2E_P01ProtectInit(E2E_P01ProtectStateType* StatePtr);

/**
 * @brief Initializes E2E_P01CheckStateType instance.
 * @param [out] StatePtr Address of the E2E_P01CheckStateType instance to be initialized.
 * @retval E_OK Function executed successfully.
 */
Std_ReturnType E2E_P01CheckInit(E2E_P01CheckStateType* StatePtr);

/**
 * @brief Protects the array/buffer to be transmitted using the E2E profile 1.
 * This includes checksum calculation, handling of counter and Data ID.
 * @param [in] ConfigPtr Pointer to static configuration.
 * @param [inout] StatePtr Pointer to port/data communication state.
 * @param [inout] DataPtr Pointer to Data to be transmitted.
 * @retval E_OK Function executed successfully.
 */
Std_ReturnType E2E_P01Protect(const E2E_P01ConfigType* ConfigPtr, E2E_P01ProtectStateType* StatePtr, uint8* DataPtr);

/**
 * @brief Checks the Data received using the E2E profile 1.
 * This includes CRC calculation, handling of Counter and Data ID.
 * @param [in] ConfigPtr Pointer to static configuration.
 * @param [inout] StatePtr Pointer to port/data communication state.
 * @param [in] DataPtr Pointer to received data.
 * @retval E_OK Function executed successfully.
 * @retval E2E_E_INPUTERR_WRONG Received Data counter out of range.
 */
Std_ReturnType E2E_P01Check(const E2E_P01ConfigType* Config, E2E_P01CheckStateType* State, const uint8* Data);

#endif /* E2E_P01_H */
