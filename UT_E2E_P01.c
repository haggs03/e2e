/** ==================================================================================================================*\
  @file UT_E2E_P01.c


  @brief Unit tests of E2E Profile 1
\*====================================================================================================================*/
#include "Std_Types.h"

#define FFF_ARG_HISTORY_LEN 4
#define FFF_CALL_HISTORY_LEN 4

#define UT_E2E_P01_TEST
#include "E2E_P01.c"

#include "acutest.h"
#include "fff.h"

#include <stdio.h>

#define TEST_DATA_LEN 96

DEFINE_FFF_GLOBALS;

FAKE_VALUE_FUNC(uint8, E2E_P01_CalculateCrcOverData_mock, const E2E_P01ConfigType*, uint8, const uint8*);
FAKE_VALUE_FUNC(uint8, Crc_CalculateCRC8, const uint8*, uint32, uint8, boolean);

/**
 *  @brief Test_Reset_Data
 *
 * 
 *  Function reseting data variable
 */

void Test_Reset_Data(uint8* data, uint16 data_len) {
    for (uint16 i = 0; i < data_len; i++) {
        data[i] = 0;
    }
}

/**
 *  @brief E2E_P01_CalculateCrcOverData
 *
 *  Test of function E2E_P01_CalculateCrcOverData
 */

void Test_Of_E2E_P01_CalculateCrcOverData(void)
{
    Std_ReturnType retv;

    E2E_P01ConfigType config;
    uint8 counter;
    uint8 data[TEST_DATA_LEN];

    uint8* DataID1byte = (uint8*)&(config.DataID);
    uint8* DataID2byte = ((uint8*)&(config.DataID)) + 1;

    uint8 CRC_OUT_fake[4] = { 10, 20, 30, 40 };

    //----------------------------------------------------------------------------------------------------------------------------------------------
    // CASE01
    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    RESET_FAKE(Crc_CalculateCRC8);
    SET_RETURN_SEQ(Crc_CalculateCRC8, CRC_OUT_fake, 4);
    config.CRCOffset = 16;
    config.DataLength = 32;
    config.DataIDMode = E2E_P01_DATAID_BOTH;
    config.DataID = 0;

    retv = E2E_P01_CalculateCrcOverData(&config, counter, data);

    TEST_CHECK(Crc_CalculateCRC8_fake.call_count = 4);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[0] == DataID1byte);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[0] == 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[0] == 0xFF);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[0] == FALSE);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[1] == DataID2byte);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[1] == 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[1] == CRC_OUT_fake[0]);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[1] == FALSE);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[2] == data);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[2] == config.CRCOffset / 8);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[2] == CRC_OUT_fake[1]);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[2] == FALSE);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[3] == &data[config.CRCOffset / 8 + 1]);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[3] == config.DataLength / 8 - config.CRCOffset / 8 - 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[3] == CRC_OUT_fake[2]);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[3] == FALSE);

    TEST_CHECK(retv == (CRC_OUT_fake[3] ^ 0xFF));

    //----------------------------------------------------------------------------------------------------------------------------------------------
    // CASE02
    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    RESET_FAKE(Crc_CalculateCRC8);
    SET_RETURN_SEQ(Crc_CalculateCRC8, CRC_OUT_fake, 1);
    config.DataIDMode = E2E_P01_DATAID_LOW;
    config.CRCOffset = 7;
    config.DataLength = 8;
    config.DataID = 0;


    retv = E2E_P01_CalculateCrcOverData(&config, counter, data);

    TEST_CHECK(Crc_CalculateCRC8_fake.call_count = 1);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[0] == DataID1byte);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[0] == 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[0] == 0xFF);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[0] == FALSE);

    TEST_CHECK(retv == (CRC_OUT_fake[0] ^ 0xFF));
    //----------------------------------------------------------------------------------------------------------------------------------------------
    // CASE0301
    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    RESET_FAKE(Crc_CalculateCRC8);
    SET_RETURN_SEQ(Crc_CalculateCRC8, CRC_OUT_fake, 1);
    counter = 2;
    config.DataIDMode = E2E_P01_DATAID_ALT;
    config.CRCOffset = 7;
    config.DataLength = 8;
    config.DataID = 12;

    retv = E2E_P01_CalculateCrcOverData(&config, counter, data);

    TEST_CHECK(Crc_CalculateCRC8_fake.call_count = 1);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[0] == DataID1byte);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[0] == 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[0] == 0xFF);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[0] == FALSE);

    TEST_CHECK(retv == (CRC_OUT_fake[0] ^ 0xFF));

    //----------------------------------------------------------------------------------------------------------------------------------------------
    // CASE0302
    //----------------------------------------------------------------------------------------------------------------------------------------------

    Test_Reset_Data(data, TEST_DATA_LEN);
    RESET_FAKE(Crc_CalculateCRC8);
    SET_RETURN_SEQ(Crc_CalculateCRC8, CRC_OUT_fake, 1);
    counter = 3;
    config.DataIDMode = E2E_P01_DATAID_ALT;
    config.CRCOffset = 7;
    config.DataLength = 8;
    config.DataID = 0;

    retv = E2E_P01_CalculateCrcOverData(&config, counter, data);

    TEST_CHECK(Crc_CalculateCRC8_fake.call_count = 1);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[0] == DataID2byte);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[0] == 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[0] == 0xFF);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[0] == FALSE);

    TEST_CHECK(retv == (CRC_OUT_fake[0] ^ 0xFF));

    //----------------------------------------------------------------------------------------------------------------------------------------------
    // CASE04
    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    RESET_FAKE(Crc_CalculateCRC8);
    SET_RETURN_SEQ(Crc_CalculateCRC8, CRC_OUT_fake, 2);
    config.DataIDMode = E2E_P01_DATAID_NIBBLE;
    config.CRCOffset = 7;
    config.DataLength = 8;
    config.DataID = 0;

    retv = E2E_P01_CalculateCrcOverData(&config, counter, data);

    TEST_CHECK(Crc_CalculateCRC8_fake.call_count = 2);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg0_history[0] == DataID1byte);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[0] == 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[0] == 0xFF);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[0] == FALSE);

    TEST_CHECK(Crc_CalculateCRC8_fake.arg1_history[1] == 1);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg2_history[1] == CRC_OUT_fake[0]);
    TEST_CHECK(Crc_CalculateCRC8_fake.arg3_history[1] == FALSE);

    TEST_CHECK(retv == (CRC_OUT_fake[1] ^ 0xFF));
}

/**
 *  @brief E2E_P01Protect
 *
 *  Test of function E2E_P01Protect
 */

void Test_Of_E2E_P01Protect(void)
{
    Std_ReturnType retv;

    E2E_P01ConfigType config;
    E2E_P01ProtectStateType state_old, state;
    uint8 data[TEST_DATA_LEN];

    uint8 (*E2E_P01_CalculateCrcOverData_tmp)(const E2E_P01ConfigType*, uint8, const uint8*) = E2E_P01_CalculateCrcOverData;
    E2E_P01_CalculateCrcOverData = E2E_P01_CalculateCrcOverData_mock;

    //----------------------------------------------------------------------------------------------------------------------------------------------

    Test_Reset_Data(data, TEST_DATA_LEN);
    config.CounterOffset = 32;
    config.DataIDMode = E2E_P01_DATAID_NIBBLE;
    config.DataIDNibbleOffset = 8;
    config.DataID = 0;
    config.CRCOffset = 60;

    state.Counter = 3;
    state_old.Counter = 3;

    E2E_P01_CalculateCrcOverData_mock_fake.return_val = 20;
    retv = E2E_P01Protect(&config, &state, data);
    TEST_CHECK(data[config.CounterOffset / 8] == ((data[config.CounterOffset / 8] & 0xF0) | (state_old.Counter & 0x0F)));
    TEST_CHECK(data[config.DataIDNibbleOffset / 8] == ((data[config.DataIDNibbleOffset / 8] & 0xF0) | (config.DataID & 0x0F)));
    TEST_CHECK(data[config.CRCOffset / 8] == E2E_P01_CalculateCrcOverData_mock_fake.return_val);
    TEST_CHECK(state.Counter == state_old.Counter + 1);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------

    Test_Reset_Data(data, TEST_DATA_LEN);
    config.CounterOffset = 7;
    config.DataIDMode = E2E_P01_DATAID_NIBBLE;
    config.DataIDNibbleOffset = 25;
    config.DataID = 0;
    config.CRCOffset = 60;

    state.Counter = 3;
    state_old.Counter = 3;

    E2E_P01_CalculateCrcOverData_mock_fake.return_val = 20;
    retv = E2E_P01Protect(&config, &state, data);
    TEST_CHECK(data[config.CounterOffset / 8] == ((data[config.CounterOffset / 8] & 0x0F) | (state_old.Counter & 0xF0)));
    TEST_CHECK(data[config.DataIDNibbleOffset / 8] == ((data[config.DataIDNibbleOffset / 8] & 0x0F) | (config.DataID & 0xF0)));
    TEST_CHECK(data[config.CRCOffset / 8 ] == E2E_P01_CalculateCrcOverData_mock_fake.return_val);
    TEST_CHECK(state.Counter == state_old.Counter + 1);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------

    Test_Reset_Data(data, TEST_DATA_LEN);
    config.CounterOffset = 7;
    config.DataIDMode = E2E_P01_DATAID_LOW;
    config.DataIDNibbleOffset = 25;
    config.DataID = 0;
    config.CRCOffset = 60;

    state.Counter = 3;
    state_old.Counter = 3;

    E2E_P01_CalculateCrcOverData_mock_fake.return_val = 20;
    retv = E2E_P01Protect(&config, &state, data);
    TEST_CHECK(data[config.CounterOffset / 8] == ((data[config.CounterOffset / 8] & 0x0F) | (state_old.Counter & 0xF0)));
    TEST_CHECK(data[config.DataIDNibbleOffset / 8] == 0);
    TEST_CHECK(data[config.CRCOffset / 8 ]== E2E_P01_CalculateCrcOverData_mock_fake.return_val);
    TEST_CHECK(state.Counter == state_old.Counter + 1);
    TEST_CHECK(retv == E_OK);

    E2E_P01_CalculateCrcOverData = E2E_P01_CalculateCrcOverData_tmp;
}

/**
 *  @brief E2E_P01ProtectInit
 *
 *  Test of function E2E_P01ProtectInit
 */

void Test_Of_E2E_P01ProtectInit(void)
{
    Std_ReturnType retv;

    E2E_P01ProtectStateType state;

    state.Counter = 1;

    retv = E2E_P01ProtectInit(&state);
    TEST_CHECK(state.Counter == 0);
    TEST_CHECK(retv == E_OK);
}

/**
 *  @brief E2E_P01CheckInit
 *
 *  Test of function E2E_P01CheckInit
 */

void Test_Of_E2E_P01CheckInit(void)
{
    Std_ReturnType retv;

    E2E_P01CheckStateType state;

    state.LastValidCounter = 1;
    state.MaxDeltaCounter = 1;
    state.WaitForFirstData = FALSE;
    state.NewDataAvailable = FALSE;
    state.LostData = 1;
    state.Status = E2E_P01STATUS_OK;
    state.NoNewOrRepeatedDataCounter = 1;
    state.SyncCounter = 1;

    retv = E2E_P01CheckInit(&state);
    TEST_CHECK(state.LastValidCounter == 0);
    TEST_CHECK(state.MaxDeltaCounter == 0);
    TEST_CHECK(state.WaitForFirstData == TRUE);
    TEST_CHECK(state.NewDataAvailable == TRUE);
    TEST_CHECK(state.LostData == 0);
    TEST_CHECK(state.Status == E2E_P01STATUS_NONEWDATA);
    TEST_CHECK(state.NoNewOrRepeatedDataCounter == 0);
    TEST_CHECK(state.SyncCounter == 0);
    TEST_CHECK(retv == E_OK);
}

/**
 *  @brief E2E_P01Check
 *
 *  Test of function E2E_P01Check
 */

void Test_Of_E2E_P01Check(void)
{
    Std_ReturnType retv;

    E2E_P01ConfigType config;
    E2E_P01CheckStateType state;
    uint8 data[TEST_DATA_LEN];
    
    uint8(*E2E_P01_CalculateCrcOverData_tmp)(const E2E_P01ConfigType*, uint8, const uint8*) = E2E_P01_CalculateCrcOverData;
    E2E_P01_CalculateCrcOverData = E2E_P01_CalculateCrcOverData_mock;

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NoNewOrRepeatedDataCounter = 10;
    state.NewDataAvailable = FALSE;
    retv = E2E_P01Check(&config, &state, data);   

    TEST_CHECK(state.Status == E2E_P01STATUS_NONEWDATA);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    config.CounterOffset = 64;
    data[(config.CounterOffset / 8) ] = 255;
    retv = E2E_P01Check(&config, &state, data);
    TEST_CHECK(retv == E2E_E_INPUTERR_WRONG);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
   
    config.DataIDMode = E2E_P01_DATAID_NIBBLE;
    config.DataIDNibbleOffset = 8;
    E2E_P01_CalculateCrcOverData_mock_fake.return_val = 9;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_WRONGCRC);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataID = 17;
    config.DataIDMode = E2E_P01_DATAID_NIBBLE;
    config.DataIDNibbleOffset = 8;
    E2E_P01_CalculateCrcOverData_mock_fake.return_val = 10;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_WRONGCRC);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = TRUE;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataID = 17;
    config.DataIDMode = E2E_P01_DATAID_NIBBLE;
    config.DataIDNibbleOffset = 7;
    E2E_P01_CalculateCrcOverData_mock_fake.return_val = 10;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_INITIAL);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = TRUE;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_INITIAL);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 0;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_REPEATED);
    TEST_CHECK(retv == E_OK);
   
    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 14;
    state.NoNewOrRepeatedDataCounter = 10;
    config.MaxNoNewOrRepeatedData = 3;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_SYNC);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 14;
    state.NoNewOrRepeatedDataCounter = 3;
    state.SyncCounter = 1;
    config.MaxNoNewOrRepeatedData = 10;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_SYNC);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 14;
    state.NoNewOrRepeatedDataCounter = 3;
    state.SyncCounter = 0;
    config.MaxNoNewOrRepeatedData = 10;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_OK);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 10;
    state.MaxDeltaCounter = 10;
    state.NoNewOrRepeatedDataCounter = 10;
    config.MaxNoNewOrRepeatedData = 3;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_SYNC);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 10;
    state.MaxDeltaCounter = 10;
    state.NoNewOrRepeatedDataCounter = 3;
    state.SyncCounter = 1;
    config.MaxNoNewOrRepeatedData = 10;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_SYNC);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 10;
    state.MaxDeltaCounter = 10;
    state.NoNewOrRepeatedDataCounter = 3;
    state.SyncCounter = 0;
    config.MaxNoNewOrRepeatedData = 10;
    config.CounterOffset = 16;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_OKSOMELOST);
    TEST_CHECK(retv == E_OK);

    //----------------------------------------------------------------------------------------------------------------------------------------------
    Test_Reset_Data(data, TEST_DATA_LEN);
    state.NewDataAvailable = TRUE;
    state.WaitForFirstData = FALSE;
    state.LastValidCounter = 1;
    state.MaxDeltaCounter = 5;
    state.SyncCounter = 1;
    config.CounterOffset = 11;
    config.CRCOffset = 24;
    data[config.CounterOffset / 8] = 0;
    data[config.CRCOffset / 8] = 10;
    data[config.DataIDNibbleOffset / 8] = 1;
    config.DataIDMode = E2E_P01_DATAID_LOW;

    retv = E2E_P01Check(&config, &state, data);

    TEST_CHECK(state.Status == E2E_P01STATUS_WRONGSEQUENCE);
    TEST_CHECK(retv == E_OK);

    E2E_P01_CalculateCrcOverData = E2E_P01_CalculateCrcOverData_tmp;
}

/*
  Lista testów - wpisz tutaj wszystkie funkcje które mają być wykonane jako testy.
  Format to { "nazwa testu", nazwa_funkcji }
*/
TEST_LIST = {
    { "Test of Test_Of_E2E_P01ProtectInit", Test_Of_E2E_P01ProtectInit },
    { "Test of Test_Of_E2E_P01CheckInit", Test_Of_E2E_P01CheckInit },
    { "Test of Test_Of_E2E_P01Protect", Test_Of_E2E_P01Protect },
    { "Test of Test_Of_E2E_P01_CalculateCrcOverData", Test_Of_E2E_P01_CalculateCrcOverData },
    { "Test of Test_Of_E2E_P01Check", Test_Of_E2E_P01Check },
    { NULL, NULL }                                        /* To musi być na końcu */
};
