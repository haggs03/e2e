#include "E2E_P01.h"
#include "Std_Types.h"
#include "Platform_Types.h"
#include "Crc.h"

/**
 * @brief Verifies the counter during data reception.
 * @param [in] ConfigPtr Pointer to static configuration.
 * @param [inout] StatePtr Pointer to port/data communication state.
 * @param [in] ReceivedCounter Counter from Data.
 * @return Nothing.
 */
inline static void E2E_P01CheckVerifyCounter(const E2E_P01ConfigType* Config, E2E_P01CheckStateType* State, uint8 ReceivedCounter)
{
    uint8 DeltaCounter;

    if(ReceivedCounter >= State->LastValidCounter) {
        DeltaCounter = ReceivedCounter - State->LastValidCounter;
    } else {
        DeltaCounter = 15 + ReceivedCounter - State->LastValidCounter;
    }
    if(DeltaCounter == 0) {
        if(State->NoNewOrRepeatedDataCounter < 14) {
            State->NoNewOrRepeatedDataCounter++;
        }
        State->Status = E2E_P01STATUS_REPEATED;
    } else if(DeltaCounter == 1) {
        State->MaxDeltaCounter = Config->MaxDeltaCounterInit;
        State->LastValidCounter = ReceivedCounter;
        State->LostData = 0;
        if(State->NoNewOrRepeatedDataCounter <= Config->MaxNoNewOrRepeatedData) {
            State->NoNewOrRepeatedDataCounter = 0;
            if(State->SyncCounter > 0) {
                State->SyncCounter--;
                State->Status = E2E_P01STATUS_SYNC;
            } else {
                State->Status = E2E_P01STATUS_OK;
            }
        } else {
            State->NoNewOrRepeatedDataCounter = 0;
            State->SyncCounter = Config->SyncCounterInit;
            State->Status = E2E_P01STATUS_SYNC;
        }
    } else if(DeltaCounter <= State->MaxDeltaCounter) {
        State->MaxDeltaCounter = Config->MaxDeltaCounterInit;
        State->LastValidCounter = ReceivedCounter;
        State->LostData = DeltaCounter - 1;
        if(State->NoNewOrRepeatedDataCounter <= Config->MaxNoNewOrRepeatedData) {
            State->NoNewOrRepeatedDataCounter = 0;
            if(State->SyncCounter > 0) {
                State->SyncCounter--;
                State->Status = E2E_P01STATUS_SYNC;
            } else {
                State->Status = E2E_P01STATUS_OKSOMELOST;
            }
        } else {
            State->NoNewOrRepeatedDataCounter = 0;
            State->SyncCounter = Config->SyncCounterInit;
            State->Status = E2E_P01STATUS_SYNC;
        }
    } else {
        State->NoNewOrRepeatedDataCounter = 0;
        State->SyncCounter = Config->SyncCounterInit;
        if(State->SyncCounter > 0) {
            State->MaxDeltaCounter = Config->MaxDeltaCounterInit;
            State->LastValidCounter = ReceivedCounter;
        }
        State->Status = E2E_P01STATUS_WRONGSEQUENCE;
    }
}

/****************************************************************************************************************************************************************/

/**
 * @brief Uses Crc Library to calculate CRC over given data.
 * @param [in] ConfigPtr Pointer to static configuration.
 * @param [in] Counter Current Counter value for alternating CRC calculation mode (E2E_P01_DATAID_ALT).
 * @param [in] DataPtr Pointer to Data for calculation.
 * @return Calculated CRC.
 */
#ifdef UT_E2E_P01_TEST
static uint8 E2E_P01_CalculateCrcOverData_original(const E2E_P01ConfigType* ConfigPtr, uint8 Counter, const uint8* DataPtr)
#else
static uint8 E2E_P01_CalculateCrcOverData(const E2E_P01ConfigType* ConfigPtr, uint8 Counter, const uint8* DataPtr)
#endif
{
    uint8 CRC;
    uint8* DataIDptr = (uint8*) &(ConfigPtr->DataID);
    uint8 zero_val = 0;

    switch(ConfigPtr->DataIDMode)
    {
        case E2E_P01_DATAID_BOTH:
            CRC = Crc_CalculateCRC8(DataIDptr, 1, 0xFF, FALSE);
            CRC = Crc_CalculateCRC8(DataIDptr + 1, 1, CRC, FALSE);
            break;

        case E2E_P01_DATAID_LOW:
            CRC = Crc_CalculateCRC8(DataIDptr, 1, 0xFF, FALSE);
            break;

        case E2E_P01_DATAID_ALT:
            if(Counter % 2 == 0) {
                CRC = Crc_CalculateCRC8(DataIDptr, 1, 0xFF, FALSE);
            } else {
                CRC = Crc_CalculateCRC8(DataIDptr + 1, 1, 0xFF, FALSE);
            }
            break;

        case E2E_P01_DATAID_NIBBLE:
            CRC = Crc_CalculateCRC8(DataIDptr, 1, 0xFF, FALSE);
            CRC = Crc_CalculateCRC8(&zero_val, 1, CRC, FALSE);
            break;
    }

    if(ConfigPtr->CRCOffset >= 8) {
        CRC = Crc_CalculateCRC8(DataPtr, (ConfigPtr->CRCOffset/8), CRC, FALSE);
    }

    if(ConfigPtr->CRCOffset/8 < (ConfigPtr->DataLength/8) - 1) {
        CRC = Crc_CalculateCRC8(&DataPtr[ConfigPtr->CRCOffset/8 + 1], ConfigPtr->DataLength/8 - ConfigPtr->CRCOffset/8 - 1, CRC, FALSE);
    }
    CRC = CRC ^ 0xFF;
    return CRC;
}

#ifdef UT_E2E_P01_TEST
uint8 (*E2E_P01_CalculateCrcOverData)(const E2E_P01ConfigType*, uint8, const uint8*) = E2E_P01_CalculateCrcOverData_original;
#endif

/****************************************************************************************************************************************************************/

Std_ReturnType E2E_P01Protect(const E2E_P01ConfigType* ConfigPtr, E2E_P01ProtectStateType* StatePtr, uint8* DataPtr)
{
    uint8 CRC;

    if(ConfigPtr->CounterOffset % 8 == 0) {
        *(DataPtr + (ConfigPtr->CounterOffset/8)) = (*(DataPtr + (ConfigPtr->CounterOffset/8)) & 0xF0) | (StatePtr->Counter & 0x0F);
    } else {
        *(DataPtr + (ConfigPtr->CounterOffset/8)) = (*(DataPtr + (ConfigPtr->CounterOffset/8)) & 0x0F) | (StatePtr->Counter & 0xF0);
    }

    if(ConfigPtr->DataIDMode == E2E_P01_DATAID_NIBBLE) {
        if(ConfigPtr->DataIDNibbleOffset % 8 == 0) {
            *(DataPtr + (ConfigPtr->DataIDNibbleOffset/8)) = (*(DataPtr + (ConfigPtr->DataIDNibbleOffset/8)) & 0xF0) | ((ConfigPtr->DataID >> 8) & 0x0F);
        } else {
            *(DataPtr + (ConfigPtr->DataIDNibbleOffset/8)) = (*(DataPtr + (ConfigPtr->DataIDNibbleOffset/8)) & 0x0F) | ((ConfigPtr->DataID >> 4) & 0xF0);
        }
    }

    CRC = E2E_P01_CalculateCrcOverData(ConfigPtr, StatePtr->Counter, DataPtr);
    *(DataPtr + (ConfigPtr->CRCOffset/8)) = CRC;
    StatePtr->Counter = (StatePtr->Counter + 1) % 15;

    return E_OK;
}

/****************************************************************************************************************************************************************/

Std_ReturnType E2E_P01ProtectInit(E2E_P01ProtectStateType* StatePtr)
{
    StatePtr->Counter = 0;
    return E_OK;
}

/****************************************************************************************************************************************************************/

Std_ReturnType E2E_P01Check(const E2E_P01ConfigType* Config, E2E_P01CheckStateType* State, const uint8* Data)
{

    if(State->MaxDeltaCounter < 14) {
        State->MaxDeltaCounter++;
    } else {
        State->MaxDeltaCounter = 14;
    }

    if(State->NewDataAvailable == TRUE) {
        uint8 ReceivedCounter, ReceivedCRC, CalculatedCRC;
        uint16 ReceivedDataIDNibble;
        if((Config->CounterOffset % 8) == 0) {
            ReceivedCounter = *(Data + (Config->CounterOffset/8)) & 0x0F;
        } else {
            ReceivedCounter = (*(Data + (Config->CounterOffset/8)) >> 4) & 0x0F;
        }

        if(ReceivedCounter < 15) {
            ReceivedCRC = *(Data + (Config->CRCOffset/8));
        } else {
            return E2E_E_INPUTERR_WRONG;
        }

        if(Config->DataIDMode == E2E_P01_DATAID_NIBBLE) {
            if((Config->DataIDNibbleOffset % 8) == 0) {
                ReceivedDataIDNibble = *(Data + (Config->DataIDNibbleOffset/8)) & 0x0F;
            } else {
                ReceivedDataIDNibble = (*(Data + (Config->DataIDNibbleOffset/8)) >> 4) & 0x0F;
            }
        }

        CalculatedCRC = E2E_P01_CalculateCrcOverData(Config, ReceivedCounter, Data);

        if(ReceivedCRC == CalculatedCRC){
            if((Config->DataIDMode == E2E_P01_DATAID_NIBBLE) && (ReceivedDataIDNibble != (Config->DataID) >> 8)) {
                State->Status = E2E_P01STATUS_WRONGCRC;
            } else {
                if(State->WaitForFirstData == TRUE) {
                    State->WaitForFirstData = FALSE;
                    State->MaxDeltaCounter = Config->MaxDeltaCounterInit;
                    State->LastValidCounter = ReceivedCounter;
                    State->Status = E2E_P01STATUS_INITIAL;
                } else {
                    E2E_P01CheckVerifyCounter(Config, State, ReceivedCounter);
                }
            }
        } else {
            State->Status = E2E_P01STATUS_WRONGCRC;
        }

    } else {
        if(State->NoNewOrRepeatedDataCounter < 14) {
            State->NoNewOrRepeatedDataCounter++;
        }
        State->Status = E2E_P01STATUS_NONEWDATA;
    }
    return E_OK;
}

/****************************************************************************************************************************************************************/

Std_ReturnType E2E_P01CheckInit(E2E_P01CheckStateType* StatePtr)
{
    StatePtr->LastValidCounter = 0;
    StatePtr->MaxDeltaCounter = 0;
    StatePtr->WaitForFirstData = TRUE;
    StatePtr->NewDataAvailable = TRUE;
    StatePtr->LostData = 0;
    StatePtr->Status = E2E_P01STATUS_NONEWDATA;
    StatePtr->NoNewOrRepeatedDataCounter = 0;
    StatePtr->SyncCounter = 0;
    return E_OK;
}
